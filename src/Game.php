<?php
/**
 * Created by PhpStorm.
 * User: Yuriy
 * Date: 25.10.2017
 * Time: 11:22
 */

namespace Soccer;

class Game
{
    /**
     * @var array
     */
    private $winner;

    /**
     * @var Team
     */
    private $firstTeam;

    /**
     * @var Team
     */
    private $secondTeam;

    /**
     * @var int
     */
    private $firstTeamGoals;
    /**
     * @var int
     */
    private $secondTeamGoals;

    /**
     * Game constructor.
     *
     * @param array $pair
     */
    public function __construct(array $pair)
    {
        list($first, $second) = $pair;
        $this->firstTeam = new Team($first);
        $this->secondTeam = new Team($second);
        $this->calculateResults();
        $this->calculateWinner($pair);
    }

    /**
     * Вычисляем количество голов обеих команд
     */
    private function calculateResults()
    {
        $this->firstTeamGoals = $this->calculateGoals(
            $this->firstTeam->getAttack(),
            $this->secondTeam->getDefense()
        );
        $this->secondTeamGoals = $this->calculateGoals(
            $this->secondTeam->getAttack(),
            $this->firstTeam->getDefense()
        );
        if ($this->firstTeamGoals - $this->secondTeamGoals == 0) {
            if (rand(0, 1) > 0) {
                $this->secondTeamGoals++;
            } else {
                $this->firstTeamGoals++;
            }
        }
    }

    /**
     * @param $attack
     * @param $defense
     *
     * @return int
     */
    private function calculateGoals($attack, $defense)
    {
        $diff = $attack - $defense;

        return ($diff > 0) ? round($diff/50) : 0;
    }

    /**
     * Вычисляем победителя
     *
     * @param $pair
     *
     */
    private function calculateWinner($pair)
    {
        $diff = $this->firstTeamGoals - $this->secondTeamGoals;
        if ($diff > 0) {
            $this->winner = $pair[0];
        } else {
            $this->winner = $pair[1];
        }
    }

    /**
     * @return array
     */
    public function getWinner()
    {
        return $this->winner;
    }

    /**
     * @return array
     */
    public function getResults()
    {
        return [
            [
                'name'  => $this->firstTeam->getName(),
                'goals' => $this->firstTeamGoals
            ],
            [
                'name'  => $this->secondTeam->getName(),
                'goals' => $this->secondTeamGoals
            ]
        ];
    }
}
