<?php
/**
 * Created by PhpStorm.
 * User: Yuriy
 * Date: 24.10.2017
 * Time: 1:20
 */

namespace Soccer;

interface TeamsProvider
{
    public function getTeams();
}
