<?php
/**
 * Created by PhpStorm.
 * User: Yuriy
 * Date: 24.10.2017
 * Time: 1:27
 */

namespace Soccer;

/**
 * Class CsvProvider
 * @package Soccer
 */
class CsvProvider implements TeamsProvider
{
    /**
     * Путь к csv-файлу
     * @var string
     */
    private $csv;

    /**
     * CsvProvider constructor.
     *
     * @param $filename
     */
    public function __construct($filename)
    {
        $this->csv = $filename;
    }

    /**
     * Получение данных по командам из файла
     *
     * @return array
     */
    public function getTeams()
    {
        $teams = [];
        if (($handle = fopen($this->csv, "r")) !== false) {
            $isHeader = true;
            while (($data = fgetcsv($handle, 0, ";")) !== false) {
                if ($isHeader) {
                    $isHeader = false;
                    continue;
                }
                $lastCell = array_pop($data);

                list($scored, $skipped) = explode('-', $lastCell);
                $data[] = trim($scored);
                $data[] = trim($skipped);
                $teams[] = $data;
            }
            fclose($handle);
        }

        return $teams;
    }
}
