<?php
/**
 * Created by PhpStorm.
 * User: Yuriy
 * Date: 24.10.2017
 * Time: 21:36
 */

namespace Soccer;

class Stage
{
    /**
     * @var array
     */
    private $teams;

    /**
     * @var array
     */
    private $pairs;

    /**
     * @var array;
     */
    private $games = [];

    /**
     * @var array
     */
    private $winners = [];

    /**
     * Stage constructor.
     *
     * @param Championship $championship
     */
    public function __construct(Championship $championship)
    {
        $this->teams = $championship->getTeams();
        $this->pairs = $this->getPairs();
        $this->startGames();
        $championship->setTeams($this->winners);
    }

    /**
     * @return array
     */
    private function getPairs()
    {
        shuffle($this->teams);
        return array_chunk($this->teams, 2);
    }

    /**
     * Получаем результаты матчей и победителей
     */
    private function startGames()
    {
        foreach ($this->pairs as $pair) {
            $game = new Game($pair);
            $this->games[] = $game->getResults();
            $this->winners[] = $game->getWinner();
        }
    }

    /**
     * @return array
     */
    public function getResults()
    {
        return $this->games;
    }
}
