<?php
/**
 * Created by PhpStorm.
 * User: Yuriy
 * Date: 24.10.2017
 * Time: 13:07
 */

namespace Soccer;

class Team
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $games;

    /**
     * @var int
     */
    private $wins;

    /**
     * @var int
     */
    private $draws;
    /**
     * @var int
     */
    private $losses;

    /**
     * @var int
     */
    private $scored;

    /**
     * @var int
     */
    private $skipped;

    /**
     * @var int
     */
    private $attack;

    /**
     * @var int
     */
    private $defense;

    /**
     * Team constructor.
     *
     * @param array $team
     */
    public function __construct(array $team)
    {
        list($name, $games, $wins, $draws, $losses, $scored, $skipped) = $team;
        $this->name = $name;
        $this->games = $games;
        $this->wins = $wins;
        $this->draws = $draws;
        $this->losses = $losses;
        $this->scored = $scored;
        $this->skipped = $skipped;
        $this->attack = $this->calculateAttack($games, $scored);
        $this->defense = $this->calculateDefense($games, $skipped);
    }

    /**
     * @return mixed|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getAttack()
    {
        return $this->attack;
    }

    /**
     * @return mixed
     */
    public function getDefense()
    {
        return $this->defense;
    }

    /**
     *
     * Вычисляем мощность атаки по количеству забитых голов во всех играх
     *
     * @param $games
     * @param $scored
     *
     * @return float
     */
    private function calculateAttack($games, $scored)
    {
        return round($scored / $games * 100) + round($games / 2);
    }

    /**
     * Вычисляем мощность защиты по количеству пропущенных мячей во всех играх
     *
     * @param $games
     * @param $skipped
     *
     * @return float
     */
    private function calculateDefense($games, $skipped)
    {
        return round(($games / $skipped) * 100) + round($games / 2);
    }

}