<?php
/**
 * Created by PhpStorm.
 * User: Yuriy
 * Date: 23.10.2017
 * Time: 23:22
 */

namespace Soccer;

/**
 * Class Championship
 * @package Soccer
 */
class Championship
{

    /**
     * Массив с данными по командам
     * @var array
     */
    private $teams = [];

    /**
     * Championship constructor.
     *
     * @param TeamsProvider $data
     */
    public function __construct(TeamsProvider $data)
    {
        $this->teams = $data->getTeams();
    }

    /**
     * @return array
     */
    public function getTeams()
    {
        return $this->teams;
    }

    /**
     * @param array $teams
     */
    public function setTeams(array $teams)
    {
        $this->teams = $teams;
    }

    /**
     * @return Collection
     */
    public function listTeams()
    {
        return new Collection($this->teams);
    }

    /**
     * @return Stage
     */
    public function startStage()
    {
        return new Stage($this);
    }
}
