<?php
/**
 * Created by PhpStorm.
 * User: Yuriy
 * Date: 24.10.2017
 * Time: 13:03
 */

require_once __DIR__ . '/vendor/autoload.php';

use Soccer\Championship;
use Soccer\CsvProvider;

$championship = new Championship(new CsvProvider(__DIR__ . '/data/data.csv'));
$stageNum = 0;
?>

<div class="container">
    <div class="row">
        <table class="table">
            <thead>
            <tr>
                <th>Команда</th>
                <th>Нападение</th>
                <th>Защита</th>
            </tr>
            </thead>
            <?php foreach ($championship->listTeams() as $team) : ?>
                <tr>
                    <td>
                        <?= $team->getName(); ?>
                    </td>
                    <td>
                        <?= $team->getAttack(); ?>
                    </td>
                    <td>
                        <?= $team->getDefense(); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

        <?php while (count($championship->getTeams()) > 1) : ?>
            <h2>Этап <?= ++$stageNum; ?></h2>
            <table class="table table-striped">
                <?php foreach ($championship->startStage()->getResults() as $games) : ?>
                    <tr>
                        <?php foreach ($games as $game) : ?>
                            <td>
                                <?= $game['name']; ?>
                            </td>
                            <td>
                                <?= $game['goals']; ?>
                            </td>

                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endwhile; ?>

    </div>
</div>
